
## Déroulé du cours

1. Structure du code source
2. écriture du fichier HTML sémantique
3. écriture du CSS
4. présentation de Node/NPM (package.json)
5. conversion en SASS (installation de node-sass)
6. installation de Webpack
7. Introduction de Git